------------------------------ MODULE DKR ------------------------------
EXTENDS Naturals, Sequences

(* 
    IDS is a tuple; its length matches the number of processes in the system.
    For TLA+ specification purposes, it's assumed that each process has an index that identifies it.
    IDS contains, at each index, the initial protocol id value of the process at that respective index.
    Must parameterize model with IDS, to describe existing processes.  
*)
CONSTANT IDS
        
ASSUME 
    /\ Len(IDS) \in (Nat \ {0})                             \* IDS must have a positive length, i.e. there must be at least one process in the system
    /\ \A i \in 1..Len(IDS) : IDS[i] \in Nat                \* All the elements in IDS must be Natural numbers (strictly speaking, they could be any kind of value, as long as there was a total order relation among them to establish a max)
    /\ \A i, j \in 1..Len(IDS) : IDS[i] = IDS[j] => i = j   \* All IDS elements are unique
    
VARIABLES 
    phase,    \* Maps each process to its current communication phase 
    state,    \* Maps each process to its current state
    max,      \* Maps each process to its current maximum value
    queue,    \* Maps each process to its incoming message queue
    leader    \* Maps each process to its known leader

vars == <<phase, state, max, queue, leader>>    \* all variables, for ease of specification, to avoid writing them all when they do not change

(*
    Struct representing a protocol message, for ease of specification. 
    
    Although there are two kinds of message in the protocol being specified (Message1 or M1, and Message2 or M2), 
    it was decided that the same struct would be used for both types (with a field type to distinguish them), 
    since it's easier for specification purposes if the incoming message queues make no distinction between message types.
    
    Message contains values for type, id, phase, counter.     
    
    Two "constructors" were defined for both messages", as seen further below.
*)
Message == [typeM : {1,2}, iM : Nat, phaseM : Nat, counterM : Nat]

(*
    Classical Type Invariant - Enforces type correctness for all specification variables.
*)
TypeInv == 
    /\ phase \in [1..Len(IDS) -> Nat]
    /\ state \in [1..Len(IDS) -> {"active", "waiting", "passive", "init"}]
    /\ max \in [1..Len(IDS) -> Nat]
    /\ queue \in [1..Len(IDS) -> Seq(Message)]  \* Sequences are used to preserve incoming message order, modeling the order-preserving environment required by the protocol to function
    /\ leader \in [1..Len(IDS) -> Nat] 

(*
    Initialize alll specification variables.
*)
Init ==     
    /\ phase = [i \in 1..Len(IDS) |-> 0]
    /\ state = [i \in 1..Len(IDS) |-> "init"] \* Initially, all processes are placed in an "init" state, to make it easier to detect them in this state and place the initial message in its queue
    /\ max = [i \in 1..Len(IDS) |-> IDS[i]]
    /\ queue = [i \in 1..Len(IDS) |-> << [typeM |-> 1, iM |-> 1, phaseM |-> 0, counterM |-> 0] >> ]
    /\ leader = [i \in 1..Len(IDS) |-> IDS[i]]

(*
    A Message of type 1 is built, given id, phase, and counter values. 
*)
MakeMessage1(maxMsg, phaseMsg, counter) ==
    [typeM |-> 1, iM |-> maxMsg, phaseM |-> phaseMsg, counterM |-> counter]
    
(*
     Message of type 2 is built with a given id value.
*)
MakeMessage2(maxMsg) ==
    [typeM |-> 2, iM |-> maxMsg, phaseM |-> 0, counterM |-> 0]

(*
    Send a message from a given process to the one immedately to "its right", i.e. the one with the next index 
    in the ring network, taking care to wrap around. This also consumes a message from the sending process' message 
    queue (the sending and the receiving were both condensed here this way because queue is a mapping for all 
    message queues, and can only be altered once per next step).
*)
SendTo(from_id, msg) ==
    LET next_id ==
        IF from_id = Len(IDS) THEN
            1
        ELSE 
            from_id + 1
    IN
        queue' = [queue EXCEPT ![next_id] = Append(queue[next_id], msg), ![from_id] = Tail(queue[from_id])]

(*
    Change the state of a process with given index to "active", and send initial protocol message to neighbor process.   
*)             
ChangeToActive(idx) == 
    /\ state' = [state EXCEPT ![idx] = "active"]
    /\ SendTo(idx, MakeMessage1(max[idx], phase[idx], 2^phase[idx]))

(*
    This operator simultaneously updates a process' current max value, and if deemed necessary 
    (the max value was received twice: the first time was to update the current max value; 
    the second time was once the value went fully around the ring, to confirm that the max value 
    comes from the the leader), updates its known protocol leader.
*)
UpdateMaxAndLeader(idx, newMax) ==
    /\ max' = [max EXCEPT ![idx] = newMax]
    /\ IF max[idx] = newMax THEN
        leader' = [leader EXCEPT ![idx] = newMax]
       ELSE
        UNCHANGED <<leader>>
  
(*
    According to the protocol description, every process in the network can be in one of three states: "active", 
    "waiting", or "passive", reacting to incoming messages in different ways, according to its state. To model
    this behavior, we defined operators to handle messages for each specific state (HandleMessageActive, 
    HandleMessageWaiting, HandleMessagePassive; see below).
*)         
(*
    Since the described protocol works in a perfect cycle, there is no way for it to start operating without 
    an external signal. To enable initialization by all processes, it was decided that all processes would start 
    in the non-protocol-specific state of "init" (with a dummy message in their queues, to be consumed once they 
    send their initial message - for ease of specification), and an additional operator (HandleMessageInit) was 
    created to merely signal the processes to start actual operation. This is, of course, irrelevant to the described 
    protocol itself, but it is meaningful for the TLA+ specification.  
*)
HandleMessageInit(idx) ==
    /\ state[idx] = "init"
    /\ ChangeToActive(idx)
    /\ UNCHANGED <<max, leader, phase>>

(*
    This operator describes the behavior of a process that is in an "active" state and receives a message from the 
    process "to its left" (neighbor_index = this_process_index - 1).
    
    An "active" process, when receiving a message of type 1, will check if the 
    message's maximum is larger than its maximum: if it is, then the process will
    update its maximum to the maximum contained in the message, and change its 
    state to "waiting"; if it is not, the process will change its state to "passive"
    and send a message of type 2, containing its own maximum.
*)
HandleMessageActive(idx) == 
    /\ queue[idx] # <<>>
    /\ Head(queue[idx]).typeM = 1
    /\ state[idx] = "active"
    /\ LET msg == Head(queue[idx]) IN
        IF msg.iM > max[idx] THEN
            /\ UpdateMaxAndLeader(idx, msg.iM)
            /\ state' = [state EXCEPT ![idx] = "waiting"]
            /\ queue' = [queue EXCEPT ![idx] = Tail(queue[idx])]
            /\ UNCHANGED <<phase>>
       ELSE
            /\ state' = [state EXCEPT ![idx] = "passive"]
            /\ SendTo(idx, MakeMessage2(max[idx]))
            /\ UNCHANGED <<max, leader, phase>>
     
(*
    This operator describes what happens to a "waiting" process once it receives a message.
    
    The "waiting" process receives a message. 
   
    If the message is of type 2, the process increments its phase and changes its state to "active". 
   
    If the message is of type 1, the process changes its state to "passive".
*)
HandleMessageWaiting(idx) ==
    /\ queue[idx] # <<>>
    /\ state[idx] = "waiting"
    /\ LET msg == Head(queue[idx]) IN
        IF msg.typeM = 2 THEN
            /\ phase' = [phase EXCEPT ![idx] = phase[idx] + 1]
            /\ ChangeToActive(idx)
            /\ UNCHANGED <<max, leader>>
        ELSE
            /\ state' = [state EXCEPT ![idx] = "passive"]
            /\ UNCHANGED <<max, leader, phase, queue>>

(* 
    This operator specifies the changes that happen once a "passive" process receives a message.
    
    The "passive" process receives a message. 
    
    If the message is of type 1, the process will behave in the following way: 
    if the message's max is greater or equal than the process' own maximum and the meessage's
    counter is greater or equal than 1, then the process' maximum is updated to the message's
    maximum; if the message's counter is greater than 1 then the process sends a message
    matching the one it received, but with a counter decremented by one; else, if the message's counter
    is equal to 1, then the process changes its state to "waiting", sends a message matching to the one 
    it received, but with a counter value of 0, and changes its phase to the one in the received message.
    
    If the received message is of type 2, the process will check if the max value contained in the
    message is larger than its own maximum; if it is, it will forward that message.         
*)     
HandleMessagePassive(idx) == 
    /\ queue[idx] # <<>>
    /\ state[idx] = "passive"
    /\ LET msg == Head(queue[idx]) IN
        IF msg.typeM = 1 THEN
            IF msg.iM >= max[idx] /\ msg.counterM >= 1 THEN
                /\ UpdateMaxAndLeader(idx, msg.iM)
                /\ IF msg.counterM > 1 THEN
                        /\ SendTo(idx, MakeMessage1(msg.iM, msg.phaseM, msg.counterM - 1))
                        /\ UNCHANGED <<phase, state>>
                    ELSE IF msg.counterM = 1 THEN
                        /\ state' = [state EXCEPT ![idx] = "waiting"]
                        /\ SendTo(idx, MakeMessage1(msg.iM, msg.phaseM, 0))
                        /\ phase' = [phase EXCEPT ![idx] = msg.phaseM]
                     ELSE
                        /\ queue' = [queue EXCEPT ![idx] = Tail(queue[idx])]
                        /\ UNCHANGED <<phase, state>>
            ELSE
                /\ SendTo(idx, MakeMessage1(msg.iM, msg.phaseM, 0))
                /\ UNCHANGED <<phase, state, max, leader>>
          ELSE
            IF msg.iM < max[idx] THEN
                /\ queue' = [queue EXCEPT ![idx] = Tail(queue[idx])]
                /\ UNCHANGED <<phase, state, max, leader>>
            ELSE
                /\ SendTo(idx, MakeMessage2(msg.iM))
                /\ UNCHANGED <<phase, state, max, leader>>                

(*
    This operator drops a message from a given process' incoming message queue. 
    This is to be used to simulate a non-reliable communication environment where messages can be lost.
    
    Once DropMessage is enabled, the system reaches a deadlock state, meaning it will  no longer progress; 
    as such none of the protocol properties (Termination, Agreement, Uniqueness) hold, since the protocol never finishes.
*)
DropRandomMessage(idx) ==
    /\ state[idx] /= "init"
    /\ queue[idx] /= <<>>
    /\ queue' = [queue EXCEPT ![idx] = Tail(queue[idx])]
    /\ UNCHANGED <<phase, max, state, leader>>

(*
    Next specifies the next-state transition. At any given point, some process may evolve according to the 
    previously specified operators.
*)
Next ==
    \E i \in 1..Len(IDS): 
        \/ HandleMessageInit(i)
        \/ HandleMessageActive(i)
        \/ HandleMessageWaiting(i)
        \/ HandleMessagePassive(i)
        \* \/ DropRandomMessage(i) \* deadlock reached if this is enabled; uncomment to try it
                
(*
    Overall Specification. For any given process, Init happens at first, and it's always possible for a process
    to evolve according to Next, or remain unchanged. It's also specified here what is the fairness of the
    previously defined operators for protocol behavior: strong fairness for all operators, which is the fairness 
    level that imposes the least amount of restriction on the actions: it's only necessary for them to be infintely 
    periodically available to be executed. 
*)     
Spec == 
    /\ Init
    /\ [][Next]_vars
    /\ \A i \in 1..Len(IDS) : 
        /\ SF_vars(HandleMessageInit(i)) 
        /\ SF_vars(HandleMessageActive(i)) 
        /\ SF_vars(HandleMessagePassive(i)) 
        /\ SF_vars(HandleMessageWaiting(i))

(*
    Below are the relevant properties that are ensured by the specified protocol. 
    These properties only hold as long as the communication medium is reliable, i.e. 
    if there are no guarantees of message delivery and order, the properties are not ensured.
*)
(*
   The Agreement property dictates that, eventually, all the processes in the network will agree on a common leader.
*)
Agreement == <>[](\A a, b \in 1..Len(IDS) : leader[a] = leader[b])

(*
    The Uniqueness property specifies that, eventually, there will only be one process 
    in the network that considers itself leader.
*)
Uniqueness == <>[](\A a, b \in 1..Len(IDS) : leader[a] = IDS[a] /\ leader[b] = IDS[b] => a = b)

(*
    The termination property specifies that the protocol finishes in a finite amount of time.
    It is the same as the Agreement property, since the termination condition is agreement.
*)
Termination == <>[](\A a, b \in 1..Len(IDS) : leader[a] = leader[b])

(* 
    End execution once the leader is discovered. For debug purposes.
*)
\* HardLimit == \E a, b \in 1..Len(IDS) : leader[a] /= leader[b] 

(*
    Theorems to prove according to this specification. 
    All protocol properties are included, as well as standard type correctness.    
*)
THEOREM Spec => []TypeInv 
THEOREM Spec => Agreement
THEOREM Spec => Uniqueness
THEOREM Spec => Termination 
=============================================================================

